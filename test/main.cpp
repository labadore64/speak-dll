#include <iostream>
#include <thread>
#include <chrono>

#include "../Speech/speechio.h"

int main()
{
    std::cerr << "initializing" << std::endl;
    speechio::init(speechio::backend_sapi);

    /* talky talky */
    std::cerr << "dispatch" << std::endl;
    speechio::speak("Now this is what I call an interruptable sentence, fellas.", speechio::normally());

    std::cerr << "sleep 2500ms" << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(2500));

    std::cerr << "dispatching speed tests" << std::endl;

    std::cerr << "dispatch" << std::endl;
    speechio::speak("Haha you have been interrupted.", speechio::speed(0.25f).immediately());
    std::cerr << "dispatch" << std::endl;
    speechio::speak("Say it really fast.", speechio::speed(0.5f));
    std::cerr << "dispatch" << std::endl;
    speechio::speak("Say it way too fast.", speechio::speed(1.0f));
    std::cerr << "dispatch" << std::endl;
    speechio::speak("Say it really slow.", speechio::speed(-0.5f));
    std::cerr << "dispatch" << std::endl;
    speechio::speak("Say it way too slow.", speechio::speed(-1.0f));
    std::cerr << "await" << std::endl;
    speechio::await(); 

    std::cerr << "dispatching team overflow test" << std::endl;
    speechio::silence(); /* force team advance */
    /* ensure no team overflow */
    speechio::speak("", speechio::immediately());
    speechio::speak("", speechio::immediately());
    speechio::speak("", speechio::immediately());
    speechio::speak("", speechio::immediately());
    speechio::speak("", speechio::immediately());
    speechio::speak("", speechio::immediately());
    speechio::speak("", speechio::immediately());
    speechio::speak("", speechio::immediately());
    speechio::speak("", speechio::immediately());
    speechio::speak("", speechio::immediately());

    std::cerr << "awaiting team overflow test" << std::endl;
    speechio::await();

    std::cerr << "dispatching pitch tests" << std::endl;

    /* pitch tests, fluent parameter pattern example */
    std::cerr << "dispatch" << std::endl;
    speechio::speak("Say it really high.", speechio::speed(0.25f).pitch(0.5f));
    std::cerr << "dispatch" << std::endl;
    speechio::speak("Say it way too high.", speechio::speed(0.5f).pitch(1.0f));
    std::cerr << "dispatch" << std::endl;
    speechio::speak("Say it really low.", speechio::speed(-0.25f).pitch(-0.5f));
    std::cerr << "dispatch" << std::endl;
    speechio::speak("Say it way too low.", speechio::speed(-0.5f).pitch(-1.0f));
    std::cerr << "await" << std::endl;
    speechio::await();

    speechio::uninit();

    return 0;
}

