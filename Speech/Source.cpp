#include "pch.h"
#include "speechio.h"

/* Speak.DLL wrapper */

/* we use an int to ensure that discrete steps aren't lost due to the factor of
 * five appearing in the unit of ten */
static int g_mLegacySpeakSpeed;
static int g_mLegacySpeakVolume;
static int g_mLegacySpeakPitch;

extern "C" __declspec (dllexport) double SetRate(double value)
{
	value *= 10.0;
	switch ((value > 10.0) | ((value < -10.0) << 1))
	{
	case 0x1:
		g_mLegacySpeakSpeed = 10;
		break;
	case 0x2:
		g_mLegacySpeakSpeed = -10;
		break;
	default:
		g_mLegacySpeakSpeed = (int)round(value);
		break;
	}
	/* Don't replace this with a multiplication by 0.1 of the floating point gods will smack you up u-u */
	return (double)g_mLegacySpeakSpeed / 10.0;
}

extern "C" __declspec (dllexport) double SetVolume(double value)
{
	value *= 100.0;
	switch ((value > 100.0) | ((value < 0.0) << 1))
	{
	case 0x1:
		g_mLegacySpeakVolume = 100;
		break;
	case 0x2:
		g_mLegacySpeakVolume = 0;
		break;
	default:
		g_mLegacySpeakVolume = (int)round(value);
		break;
	}
	return (double)g_mLegacySpeakVolume / 100.0;
}

/* For completness in legacy API */
extern "C" __declspec (dllexport) double SetPitch(double value)
{
	value *= 10.0;
	switch ((value > 10.0) | ((value < -10.0) << 1))
	{
	case 0x1:
		g_mLegacySpeakPitch = 10;
		break;
	case 0x2:
		g_mLegacySpeakPitch = -10;
		break;
	default:
		g_mLegacySpeakPitch = (int)round(value);
		break;
	}
	return (double)g_mLegacySpeakPitch / 10.0;
}

/* Legacy Speak.DLL API */

extern "C" __declspec (dllexport) double SpeakString(char* string)
{
	speechio::speak(string,
		speechio::immediately()
		  .speed((float)g_mLegacySpeakSpeed / 10.0f)
		  .pitch((float)g_mLegacySpeakPitch / 10.0f)
		  .volume((float)g_mLegacySpeakVolume / 100.0f));
	//, (float)g_mLegacySpeakSpeed / 10.0f, (float)g_mLegacySpeakVolume / 100.0f, true);
	return 0.0; /* always succeed */
}

extern "C" __declspec (dllexport) double initialize()
{
	g_mLegacySpeakSpeed = 0;
	g_mLegacySpeakPitch = 0;
	g_mLegacySpeakVolume = 100;
	return (double)speechio::init(speechio::backend_default);
}

extern "C" __declspec (dllexport) double uninitialize()
{
	return (double)speechio::uninit();
}

extern "C" __declspec (dllexport) double interupt()
{
	speechio::silence();
	return 0.0; /* always succeed */
}

extern "C" __declspec (dllexport) double IncreaseRate()
{
	g_mLegacySpeakSpeed -= (++g_mLegacySpeakSpeed > 10);
	return (double)g_mLegacySpeakSpeed / 10.0;
}

extern "C" __declspec (dllexport) double DecreaseRate()
{
	g_mLegacySpeakSpeed += (--g_mLegacySpeakSpeed < -10);
	return (double)g_mLegacySpeakSpeed / 10.0;
}

/* Support pitch adjustment consistent with legacy API */
extern "C" __declspec (dllexport) double IncreasePitch()
{
	g_mLegacySpeakPitch -= (++g_mLegacySpeakPitch > 10);
	return (double)g_mLegacySpeakPitch / 10.0;
}

/* Support pitch adjustment consistent with legacy API */
extern "C" __declspec (dllexport) double DecreasePitch()
{
	g_mLegacySpeakPitch += (--g_mLegacySpeakPitch < -10);
	return (double)g_mLegacySpeakPitch / 10.0;
}

/* support volume adjustment consistent with legacy API */
extern "C" __declspec (dllexport) double IncreaseVolume()
{
	g_mLegacySpeakVolume -= (++g_mLegacySpeakVolume > 100);
	return (double)g_mLegacySpeakVolume / 100.0;
}

/* support volume adjustment consistent with legacy API */
extern "C" __declspec (dllexport) double DecreaseVolume()
{
	g_mLegacySpeakVolume += (--g_mLegacySpeakVolume < 0);
	return (double)g_mLegacySpeakVolume / 100.0;
}
