/* windows speechio 2 */

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#ifndef _SPEECHIO2_INCLUDED_
#include "speechio.h"
#endif

#include <iostream>
#include <sstream>
#include <fstream>
#include <process.h>
#include <sapi.h>
#include <atomic>
#include <string>
#include <ctime>
#include <iostream>
#include <locale>
#include <codecvt>
#include <string>
#include <cmath>

#define SPEECHIO_SAPI_TEAM_SIZE 8 /* must be a power of two */

static ::ISpVoice *g_mpVoices[SPEECHIO_SAPI_TEAM_SIZE]; /* Voice array. */
static int g_mActiveVoiceIndex;
static ::ISpVoice *g_pActiveVoice;   /* Global pointer to the current voice. */

static inline void _speechio_advance_voice_index()
{
	++g_mActiveVoiceIndex;
	g_mActiveVoiceIndex &= SPEECHIO_SAPI_TEAM_SIZE - 1;
	g_pActiveVoice = g_mpVoices[g_mActiveVoiceIndex];
}

speechio::parameters::parameters()
	: mSpeed(0.0f)
	, mVolume(1.0f)
	, mPitch(0.0f)
	, mPriority(speechio::priority_enqueue)
{ }

speechio::parameters::parameters(speechio::parameters const &o)
	: mSpeed(o.mSpeed)
	, mVolume(o.mVolume)
	, mPitch(o.mPitch)
	, mPriority(o.mPriority)
{ }

speechio::parameters::~parameters() { }

speechio::parameters &speechio::parameters::operator =(speechio::parameters const &o)
{
	mSpeed = o.mSpeed;
	mVolume = o.mVolume;
	mPitch = o.mPitch;
	mPriority = o.mPriority;
	return *this;
}

speechio::parameters &speechio::parameters::speed(float m)
{
	mSpeed = m;
	return *this;
}

speechio::parameters &speechio::parameters::volume(float m)
{
	mVolume = m;
	return *this;
}

speechio::parameters &speechio::parameters::pitch(float m)
{
	mPitch = m;
	return *this;
}

speechio::parameters &speechio::parameters::priority(speechio::epriority m)
{
	mPriority = m;
	return *this;
}

float speechio::parameters::speed() const
{
	return mSpeed;
}

float speechio::parameters::volume() const
{
	return mVolume;
}

float speechio::parameters::pitch() const
{
	return mPitch;
}

speechio::epriority speechio::parameters::priority() const
{
	return mPriority;
}

int speechio::init(speechio::ebackend mBackend)
{
	if (speechio::backend_sapi != mBackend && speechio::backend_default != mBackend)
		return -10; /* unsupported backend */

	/* init default voice and active voice */
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
	if (FAILED(hr))
	{
		std::cerr << "speechio: CoInitializeEx failed with result " << hr << std::endl;
		return -1;
	}

	for (int i = 0; i < SPEECHIO_SAPI_TEAM_SIZE; ++i)
	{
		hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice,
			(void**)&g_pActiveVoice);

		if (FAILED(hr) || NULL == g_pActiveVoice)
		{
			std::cerr << "speechio: CoCreateInstance failed for voice "
				<< i << " with result " << hr << std::endl;
			return -2;
		}

		g_mpVoices[i] = g_pActiveVoice;
	}

	g_pActiveVoice = g_mpVoices[0];
	g_mActiveVoiceIndex = 0;

	return 0;
}

int speechio::uninit()
{
	for (int i = 0; i < SPEECHIO_SAPI_TEAM_SIZE; ++i)
		g_mpVoices[i]->Release();

	return 0;
}

static std::wstring _escapexml(std::wstring const &mStr)
{
	std::wostringstream mSS;
	for (int i = 0; i < mStr.size(); ++i)
	{
		switch (mStr[i])
		{
		case L'<':
			mSS << L"&lt;";
			break;
		case L'>':
			mSS << L"&gt;";
			break;
		case L'&':
			mSS << L"&amp;";
			break;
		case L'"':
			mSS << L"&quot;";
			break;
		case L'\'':
			mSS << L"&apos;";
			break;
		default:
			mSS << mStr[i];
			break;
		}
	}
	return mSS.str();
}

template <typename T>
static inline T const &_clip(T const &x, T const &el, T const &u)
{
	switch ((x > u) | ((x < el) << 1))
	{
	case 0x1:
		return u;
		break;
	case 0x2:
		return el;
		break;
	default:
		return x;
		break;
	}
}

void speechio::speak(std::wstring const &mStr, speechio::parameters const &mParams)
{
	bool bImmediate = false;
	std::wostringstream mSS;

	switch (mParams.mPriority)
	{
	case speechio::priority_immediate:
		bImmediate = true;
		speechio::silence();
		break;
	case speechio::priority_enqueue:
		break;
	default:
		break;
	}

	/* build xml tags for volume, rate, and pitch */
	mSS << L"<volume level=\""
			<< (int)roundf(_clip(100.0f * mParams.mVolume, 0.0f, 100.0f))
			<< L"\">"
		   L"<rate absspeed=\""
			<< (int)roundf(_clip(10.0f * mParams.mSpeed, -10.0f, 10.0f))
			<< L"\">"
		   L"<pitch absmiddle=\""
			<< (int)roundf(_clip(10.0f * mParams.mPitch, -10.0f, 10.0f))
			<< L"\">"
		<< _escapexml(mStr)
		<< L"</pitch>"
		   L"</rate>"
		   L"</volume>";

	/* debug */
	/* std::wcerr << L"DISPATCH> " << mSS.str() << std::endl; */

	/* dispatch speech and return */
	if (FAILED(g_pActiveVoice->Speak(mSS.str().c_str(),
		bImmediate
			? SPF_PURGEBEFORESPEAK|SPF_ASYNC|SPF_IS_XML
			: SPF_ASYNC|SPF_IS_XML,
		NULL)))
	{
		/* just warn, don't break flow */
		std::cerr << "speechio: I'm dumb with voice "
			<< (void*)g_pActiveVoice << "!" << std::endl;
	}
}

void speechio::speak(std::string const &mText, speechio::parameters const &mParams)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	speechio::speak(converter.from_bytes(mText), mParams);
}

void speechio::speak(wchar_t const *pStr, speechio::parameters const &mParams)
{
	speechio::speak(std::wstring(pStr), mParams);
}

void speechio::speak(char const *pText, speechio::parameters const &mParams)
{
	speechio::speak(std::string(pText), mParams);
}

void speechio::silence()
{
	if (FAILED(g_pActiveVoice->Speak(NULL, SPF_PURGEBEFORESPEAK|SPF_ASYNC, NULL)))
	{
		/* just warn, don't break flow */
		std::cerr << "speechio: I can't stop with voice "
			<< (void*)g_pActiveVoice << "!" << std::endl;
	}

	/* now use the next voice in the team */
	_speechio_advance_voice_index();
}

/* returns 0 on complete, 1 on timed out */
int speechio::await_timeout(float timeout)
{
#if !defined(_MSC_VER)
	static float const _infinity = 1.0f / 0.0f;
#endif

	if (timeout < 0.0f) timeout = 0.0f;

#if defined(_MSC_VER)
	return S_FALSE == g_pActiveVoice->WaitUntilDone(
		(ULONG)roundf(timeout * 1000.0f));
#else
	return S_FALSE == g_pActiveVoice->WaitUntilDone(
		timeout == _infinity
			? INFINITE
			: (ULONG)roundf(timeout * 1000.0f));
#endif
}

int speechio::await()
{
	return S_FALSE == g_pActiveVoice->WaitUntilDone(INFINITE);
}
