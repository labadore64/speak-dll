#ifndef _SPEECHIO2_INCLUDED_
#define _SPEECHIO2_INCLUDED_

#include <string>

namespace speechio
{
    enum ebackend
    {
        backend_default = 0, /* choose for me */
        backend_espeak,      /* currently unsupported */
        backend_sapi,        /* windows native */
        backend_internal,    /* currently unsupported */
        backend_count        /* number of backends */
    };

    enum epriority
    {
        priority_immediate, /* immediately interrupt the current speech and begin speaking a new string */
        priority_enqueue    /* enqueue the string to be spoken after all other enqueued strings */
    };

    struct parameters
    {
        float mSpeed;  /* -1 to 1, -1 being the slowest and 1 being the fastest */
        float mVolume; /*  0 to 1, 0 being the softest and 1 being the loudest  */
        float mPitch;  /* -1 to 1, -1 being the lowest and 1 being the highest  */
        epriority mPriority; /* speech priority */

        parameters();
        parameters(parameters const &o);
        ~parameters();

        parameters &operator =(parameters const &o);

        /* fluent pattern continuation assignments */
        parameters &speed(float m);
        parameters &volume(float m);
        parameters &pitch(float m);
        parameters &priority(epriority m);

        inline parameters &immediately()
        {
            return priority(priority_immediate);
        }

        float speed() const;
        float volume() const;
        float pitch() const;
        epriority priority() const;
    };

    /* fluent pattern parameter base assignments */
    static inline parameters speed(float m)         { return parameters().speed(m);    }
    static inline parameters volume(float m)        { return parameters().volume(m);   }
    static inline parameters pitch(float m)         { return parameters().pitch(m);    }
    static inline parameters priority(epriority m)  { return parameters().priority(m); }

    /* nicer to look at */
    static inline parameters normally()    { return parameters(); }
    static inline parameters immediately() { return priority(priority_immediate); }

    int init(ebackend mBackend);
    int uninit();

    void speak(std::wstring const &mStr, parameters const &mParams);
    void speak(std::string const &mText, parameters const &mParams);
    void speak(wchar_t const *pStr, parameters const &mParams);
    void speak(char const *pText, parameters const &mParams);

    /* stop the currently speaking voice */
    void silence();

    /* await the completion of the currently speaking voice */
    int await_timeout(float timeout);
    int await(); /* equivalent to await_timeout(1.0f/0.0f); */
}

#endif

